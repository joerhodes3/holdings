# holdings

# about/ -- is for tracking assets held (crypto works just like security -- intertest/staking proceed taxed as they accrue, and then are cost basis)

# TODO
- 2022 audit, 2023, corp as well
- unit_test (more tests)
- Docker
- history endpoint
- Apollo (pretty JS frontend)
- analysis, assisted trade, algo trade
- postgres
- security (keycloak)
- CI/CD
- scan (Sonartype linter)

# To install deps on Ubuntu v23.04
```
source ~/venv/bin/activate
cd ~/holdings
pip -r requirements.txt
```

# To run locally
```
source ~/venv/bin/activate

rm ~/holdings/holding/assets_development.sql

python web.py
```

http://127.0.0.1:5002/apidocs/

- curl:
```
#IN <form>
#SQL
curl -X POST -d "filename=`pwd`/holding/transactions/2023_assets.csv&asset_type=have" http://127.0.0.1:5002/load/csv

#IN <form>
#SQL
curl -X POST -d "filename=`pwd`/holding/transactions/2024.csv&asset_type=year" http://127.0.0.1:5002/load/csv

#SQL (FIFO?)
curl -X GET http://127.0.0.1:5002/process

#OUT <form>
# SQL -> csv file
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assets.csv" http://127.0.0.1:5002/report/csv/asset
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assets.wiki" http://127.0.0.1:5002/report/wiki/asset
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assets.json" http://127.0.0.1:5002/report/json/asset

curl -X POST -d "filename=`pwd`/holding/transactions/2024_tax.csv&year=2023" http://127.0.0.1:5002/report/csv/tax
curl -X POST -d "filename=`pwd`/holding/transactions/2024_tax.wiki&year=2023" http://127.0.0.1:5002/report/wiki/tax
curl -X POST -d "filename=`pwd`/holding/transactions/2024_tax.txf&year=2023" http://127.0.0.1:5002/report/txf/tax
curl -X POST -d "filename=`pwd`/holding/transactions/2024_tax.json&year=2023" http://127.0.0.1:5002/report/json/tax
```





```
#IN <form>
#SQL
curl -X POST -d "filename=`pwd`/holding/transactions/2023_assorted_assets.csv&asset_type=have" http://127.0.0.1:5002/load/csv

#IN <form>
#SQL
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assorted_solutions.csv&asset_type=year" http://127.0.0.1:5002/load/csv

#SQL (FIFO?)
curl -X GET http://127.0.0.1:5002/process

#OUT <form>
# SQL -> csv file
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assorted_assets.csv" http://127.0.0.1:5002/report/csv/asset
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assorted_assets.wiki" http://127.0.0.1:5002/report/wiki/asset
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assorted_assets.json" http://127.0.0.1:5002/report/json/asset

curl -X POST -d "filename=`pwd`/holding/transactions/2024_assorted_tax.csv&year=2023" http://127.0.0.1:5002/report/csv/tax
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assorted_tax.wiki&year=2023" http://127.0.0.1:5002/report/wiki/tax
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assorted_tax.txf&year=2023" http://127.0.0.1:5002/report/txf/tax
curl -X POST -d "filename=`pwd`/holding/transactions/2024_assorted_tax.json&year=2023" http://127.0.0.1:5002/report/json/tax
```

- GraphQL
```
http://127.0.0.1:5002/graphql

{
  allLedger {
    edges {
      node {
        id
        Asset
        DateBought
      }
    }
  }
}
```
