# -*- coding: utf-8 -*-
from io import StringIO

from holding.util import (
    parse_csv,
    process_todo,
    report,
)
from holding.document import document
from holding.models import ledger, serialize

##################from sqlalchemy.sql import and_

import json


# load csv into SQL and return message
def get_csv_transactions(fname, op_type):
    csv_have = open(fname, mode="r", encoding="utf-8-sig")
    # csv minus comments in memory
    g = StringIO()
    for row in csv_have.readlines():
        raw = row.split("#")[0].strip()
        g.write(raw)
        g.write("\n")
    csv_have.close()

    if op_type != "have" and op_type != "year" and op_type != "audit":
        print(f"invalid operation {op_type}")
        return "<p>Fail</p>"
    else:
        header = "Date,Operation,Asset,Amount,Price,Exchange"
        parse_csv(g, op_type, header)  # SQL
        return "<p>Success</p>"


# future -- incoming JSON transactions


def sql_to_json(items):
    lines = []
    for item in items:
        lines.append(serialize(item.as_dict()))

    return json.dumps(lines)


def taxes(ouptput_method, fname, begin_date, end_date):
    # SQL query date range (based on year)
    # SELL, AIRDROP, INTEREST
    items = (
        ledger.query.filter(
            ##############ledger.Date_sold.between(begin_date, end_date),
            ledger.is_taxable
            == True,
        )
        .order_by(ledger.Asset.asc(), ledger.Date_bought.asc())
        .all()
    )
    tax = report(sql_to_json(items))

    status = "<p>Success</p>"
    if ouptput_method == "csv" or ouptput_method == "wiki":
        tax.calc_tax_total_gain()
        keys = [
            "Amount",
            "Asset",
            "Date_bought",
            "Date_sold",
            "Price_bought",
            "Price_sold",
            "purchased_on",
            "held_on",
            "sold_on",
            "term",
            "proceeds",
        ]
        doc = document(tax.json_to_doc_tax(keys))
        if ouptput_method == "csv":
            doc.to_csv(fname)
        if ouptput_method == "wiki":
            doc.to_wiki(fname)

    elif ouptput_method == "txf":
        tax.json_to_txf_sell(fname)

    elif ouptput_method == "json":
        tax.output_json(fname)

    else:
        status = "<p>Fail</p>"

    return status


def assets(ouptput_method, fname, begin_date, end_date):
    items = (
        ledger.query.filter_by(is_have=True)
        .order_by(ledger.Asset.asc(), ledger.Date_bought.asc())
        .all()
    )
    asset = report(sql_to_json(items))

    status = "<p>Success</p>"
    if ouptput_method == "csv" or ouptput_method == "wiki":
        keys = [
            "Date_bought",
            "Operation",
            "Asset",
            "Amount",
            "Price_bought",
            "purchased_on",
            "held_on",
        ]
        asset.process_assets()
        doc = document(asset.json_to_doc_asset(keys))
        if ouptput_method == "csv":
            doc.to_csv(fname)
        if ouptput_method == "wiki":
            doc.to_wiki(fname)

    elif ouptput_method == "json":
        asset.output_json(fname)

    else:
        status = "<p>Fail</p>"

    return status


def query_year(year):
    begin_date = str(f"{year}-01-01")
    end_date = str(f"{year}-12-31")

    return begin_date, end_date
