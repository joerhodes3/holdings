# -*- coding: utf-8 -*-
# export FLASK_ENV=development
from holding.models import db_session, Base, engine
##from holding.schema import schema, ledger, comment

from holding.routes import bp_app

from flask import Flask
#from flask_graphql import GraphQLView

from flask_cors import CORS
from flasgger import Swagger

import os


SECRET_KEY = os.urandom(32)
flask_app = Flask(__name__)
flask_app.config["CORS_HEADERS"] = "Content-Type"
# http://127.0.0.1:5002/apidocs/
swagger = Swagger(flask_app)
cors = CORS(flask_app)
flask_app.register_blueprint(bp_app)

#flask_app.add_url_rule(
#    "/graphql",
#    view_func=GraphQLView.as_view(
#        "graphql", schema=schema, graphiql=True  # for having the GraphiQL interface
#    ),
#)


@flask_app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


def config_app(test_config, app):
    if test_config == "production":
        app.config.from_mapping(
            TESTING=False,
            SECRET_KEY=SECRET_KEY,
            WTF_CSRF_ENABLED=True,
            CSRF_ENABLED=True,
        )
    elif test_config == "development":
        app.config.from_mapping(
            TESTING=False,
            SECRET_KEY=SECRET_KEY,
            WTF_CSRF_ENABLED=True,
            CSRF_ENABLED=True,
        )
    else:
        app.config.from_mapping(
            TESTING=True,
            WTF_CSRF_ENABLED=False,
            CSRF_ENABLED=False,
        )

    Base.metadata.create_all(bind=engine)

    return app
