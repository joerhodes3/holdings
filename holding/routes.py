# -*- coding: utf-8 -*-
import json

from holding.assets import get_csv_transactions, assets, taxes
from holding.util import asset_items  # for process/
from holding.forms import Input, Report

from flask import Blueprint, request, render_template, redirect, jsonify, url_for
from markupsafe import escape
from flask_json import as_json

from flask_cors import cross_origin

###from flasgger import swag_from


bp_app = Blueprint("bp_app", __name__)


@bp_app.route("/", methods=["GET"])
@cross_origin()
@as_json
def index():
    """
    Base entrypoint
    ---
    responses:
      200:
        description: all good
      500:
        description: WTF
    """
    title = "index"
    message = "Hello, this is the root.  The easiest way to talk to me is with curl."
    return render_template("message.html", title=title, message=message)


@bp_app.route("/hello", methods=["GET"])
@cross_origin()
@as_json
def hello():
    """
    hello is alive
    ---
    responses:
      200:
        description: all good
      500:
        description: WTF
    """
    return "Hello, World!"


####HealthCheck(app, "/health")


@bp_app.route("/about", methods=["GET"])
@cross_origin()
@as_json
def about():
    # is for tracking assets held (crypto works just like security -- intertest/staking proceed taxed as they accue, and then are cost basis)
    """
    hello is alive
    ---
    responses:
      200:
        description: all good
      500:
        description: WTF
    """
    return "Hello, World!"


@bp_app.route("/load/csv", methods=["GET", "POST"])
@cross_origin()
@as_json
def load_csv():
    """
    Entrypoint load into the SQL model for config given a JSON
    ---
    parameters:
      - name: filename
        type: str
        descrption: the csv file containg the assets to put in the ledger
        required: true
        default: "csv/2020_assets_after.csv"
      - name: asset_type
        type: str
        descrption: how to view file (supported -- have, year, or audit)
        required: true
        default: "have"
    responses:
      200:
        description: all good
      500:
        description: WTF
    """
    form = Input(request.form)

    if form.is_submitted():
        # regular Submit
        # print(form.filename.data)
        # print(form.asset_type.data)
        message = get_csv_transactions(form.filename.data, form.asset_type.data)

        title = "loading csv..............."
        return render_template("message.html", title=title, message=message)
    # display form
    else:
        template_args = {}
        template_args["filename"] = "holding/transactions/2020_assets_after.csv"
        template_args["asset_type"] = "have"
        template_args["title"] = "input filename"
        template_args["form"] = form
        return render_template("read_file.html", **template_args)


@bp_app.route("/process", methods=["GET"])
@cross_origin()
@as_json
def process_assets():
    """
    process -- build FIFO and then buy/sell (to create updated db to report)
    ---
    responses:
      200:
        description: all good
      500:
        description: WTF
    """
    FIFO = asset_items()
    FIFO.process_todo()

    message = "Sucessfuully processed, ready to report"
    title = "process"
    return render_template("message.html", title=title, message=message)


@bp_app.route("/report", methods=["GET"])
@bp_app.route("/report/<output>/<report_type>", methods=["GET", "POST"])
@cross_origin()
@as_json
def report(output=None, report_type=None):
    """
    report:
        {output} requires a filename to write to
        currently {report_type} is either 'tax' or 'assest'.  (tax requires year, asset is everything)
    ---
    parameters:
      - name: filename
        type: string
        descrption: the name of the output file
        required: true
        default: "2021_asset.csv"
      - name: year
        type: string
        descrption: the range to limit the tax report to
        required: false
        default: "2021"
    responses:
      200:
        description: all good
      500:
        description: WTF
    """
    # TODO make reports based on supported_formats
    # TODO template for supported report formats
    supported_formats = {
        "asset": ["wiki", "csv", "json"],
        "tax": ["wiki", "csv", "txf", "json"],
    }

    if escape(report_type) == "asset":
        form = Report(request.form)
        if form.is_submitted():
            title = "assets"
            if escape(output) in supported_formats["asset"]:
                message = assets(escape(output), form.filename.data, None, None)
                return render_template("message.html", title=title, message=message)
            else:
                title = "The supported REPORT formats are"
                return render_template(
                    "supported_reports.html",
                    title=title,
                    supported_tax=supported_formats["tax"],
                    supported_asset=supported_formats["asset"],
                )

        # display form
        else:
            template_args = {}
            template_args["title"] = "csv output for assets all time"
            template_args["form"] = form
            template_args["fname"] = "holding/transactions/2021_assets.????"
            return render_template("write_asset.html", **template_args)
    elif escape(report_type) == "tax":
        form = Report(request.form)
        if form.is_submitted():
            title = "tax"

            if escape(output) in supported_formats["tax"]:
                message = taxes(
                    escape(output),
                    form.filename.data,
                    form.year.data + "-1-1",
                    form.year.data + "-12-31",
                )
                return render_template("message.html", title=title, message=message)
            else:
                title = "The supported REPORT formats are"
                return render_template(
                    "supported_reports.html",
                    title=title,
                    supported_tax=supported_formats["tax"],
                    supported_asset=supported_formats["asset"],
                )

        # display form
        else:
            template_args = {}
            template_args[
                "title"
            ] = "output for a specific year of taxable transactions"
            template_args["form"] = form
            template_args["filename"] = "holding/transactions/2021_tax.????"
            template_args["year"] = "2023"
            return render_template("write_tax.html", **template_args)
    else:
        title = "The supported REPORT formats are"
        return render_template(
            "supported_reports.html",
            title=title,
            supported_tax=supported_formats["tax"],
            supported_asset=supported_formats["asset"],
        )
