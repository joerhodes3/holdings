# -*- coding: utf-8 -*-
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from graphene import relay, Schema, ObjectType
from holding.models import ledger as LedgerModel, comment as CommentModel


class ledger(SQLAlchemyObjectType):
    # SQLAlchemy Object Type for Ledger Type

    class Meta:
        # Set the (SQLAlchemy) model to use
        model = LedgerModel
        interfaces = (relay.Node,)


class comment(SQLAlchemyObjectType):
    # SQLAlchemy Object Type for Comment Type

    class Meta:
        # Set the (SQLAlchemy) model to use
        model = CommentModel
        interfaces = (relay.Node,)


class Query(ObjectType):
    node = relay.Node.Field()
    # Allows sorting over multiple columns, by default over the primary key
    all_ledger = SQLAlchemyConnectionField(ledger.connection)
    # Disable sorting over this field
    all_comment = SQLAlchemyConnectionField(comment.connection, sort=None)


schema = Schema(query=Query)
