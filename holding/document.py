# -*- coding: utf-8 -*-
class document:
    def __init__(self, document):
        self.document = document

    def to_csv(self, fname):
        # in:
        # - document: "[{header: "", table: [<rows [<cells>]>], footer: ""}, ...]"
        # - fname to output to
        #
        # out:
        # - a csv (one transaction per line)
        headerSeparator = ","
        bodySeparator = ","
        FileOut = open(fname, "w")
        for items in self.document:
            if (
                items["header"] == "SELL"
                or items["header"] == "BUY"
                or items["header"] == "HAVE"
            ):
                # headers
                table = items["table"]
                index = 0
                for row in table:
                    if index == 1:
                        FileOut.write(f'# {items["header"]}\n')
                    FileOut.write(f"{(headerSeparator).join(row)}\n")
                    index += 1
                continue

            elif items["header"] != "":
                FileOut.write(f'# {items["header"]}\n')
            if items["table"] != []:
                if items["header"] == "INTEREST" or items["header"] == "AIRDROP":
                    table = items["table"][1:]
                else:
                    table = items["table"]
                for rows in table:
                    FileOut.write(f"{(bodySeparator).join(rows)}\n")
            if items["footer"] != "":
                FileOut.write(f'# {items["footer"]}\n')

        FileOut.close()

    def to_wiki(self, fname):
        # in:
        # - document: "[{header: "", table: [<rows [<cells>]>], footer: ""}, ...]"
        # - fname to output to
        #
        # out:
        # - a csv (one transaction per line)
        headerSeparator = "||"
        bodySeparator = "|"
        FileOut = open(fname, "w")
        for items in self.document:
            if items["header"] == "BUY":
                continue

            if items["header"] != "":
                FileOut.write(f'# {items["header"]}\n')
            if items["table"] != []:
                table = items["table"]
                FileOut.write(
                    f"{(headerSeparator)}{(headerSeparator).join(table[0])}\n"
                )
                for rows in table[1:]:
                    FileOut.write(f"{(bodySeparator)}{(bodySeparator).join(rows)}\n")
            if items["footer"] != "":
                FileOut.write(f'- {items["footer"]}\n')
        FileOut.close()
