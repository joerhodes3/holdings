# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    validators,
    SubmitField,
)
from wtforms.validators import InputRequired, Length


class Report(FlaskForm):
    filename = StringField(
        "file that contains csv for ledger", validators=[Length(min=3, max=99)]
    )
    year = StringField(
        "limit tax report to thhis query", validators=[Length(min=0, max=4)]
    )
    save_file = SubmitField(label="Save file")


class Input(FlaskForm):
    filename = StringField(
        "file that contains csv for ledger", validators=[Length(min=3, max=99)]
    )
    asset_type = StringField("have, year, audit", validators=[Length(min=4, max=6)])
    submit = SubmitField(label="Save file")
