#!/usr/bin/python3
# -*- coding: utf-8 -*-
from build_wrapper import config_app, flask_app
from build_wrapper.models import db

import unittest

from flask.testing import FlaskClient
from flask.wrappers import Response

# sudo apt install python3-bs4
from bs4 import BeautifulSoup

import json
import os


# ---- common functions (not tests) --------
def load_config(app, json_string):
    load_url = "/load/config"
    # POST json_string
    rv = app.post(
        load_url,
        data=json.dumps(json_string),
        content_type='application/json',
    )

    return rv


def fetch_config(app):
    get_json_url = ("/fetch/config")
    rv = app.get(get_json_url)
    
    return rv
# ----


# ???? TODO make tests that can't Submit form -- wrong values -- negitive tests

# NOTE: would do a TestBase if login/
# this is POST do GET in a new class, also need to test add() and remove()
# TODO add() / view()
# TODO other buttons in config form
# TODO SQL -- update() -- as new class
class TestConfig(unittest.TestCase):
    def setUp(self):
        # Assign the flask app object
        app = config_app("testing", flask_app)
        with app.app_context():
           db.create_all()

        self.app = app.test_client()
        self.db = db

        self.maxDiff = None

        os.system("cp build_wrapper/test/config.json config.json")


    def test_hello(self):
        # is the app alive and GET
        alive_url = "/hello"

        rv = self.app.get(alive_url)

        self.assertEqual(200, rv.status_code)
        # print("****rv %s is type %s" % (rv.data,type(rv.data)))
        self.assertEqual(b"hello world!", rv.data)


    def test_read_config(self, fname='config.json'):
        read_url = ("/read/config")

        data=dict(filename=fname)
        print("**"+str(data))

        rv = self.app.post(
            read_url,
            data=dict(filename=fname),
        )
        ###self.assertEqual(b'Config read in', rv.data)
        expected_config = fetch_config(self.app)
    
        expected_json = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)


    def test_load_config(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given)
        # TODO success
    
        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)


    def test_modify_config_host(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success
    
        # json_string, what, id_num
        what = "host"
        id_num = 1
        form_data = {"host_id": 1, "hostname": "xxxx", "known_usb": '["foo1: bar1", "foo2: bar2"]'} # submit <form>
        modify_url = ("/edit/config/modify/%s/%s" % (what, id_num))
        rv = self.app.post(
            modify_url,
            data=form_data,
        )
        # TODO success

        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"xxxx": {"known_hubs": ["foo1: bar1", "foo2: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)

       # csv
        fh = open("config.json", "r")
        config_data = json.load(fh)
        fh.close()

        self.assertDictEqual(expected_json, rv)


    def test_modify_config_build_name(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success
    
        # json_string, what, id_num
        what = "build"
        id_num = 1
        form_data = {"build_id": 1, "target_name": "xxxx", "other_target_name": "sg16"} # submit <form>
        modify_url = ("/edit/config/modify/%s/%s" % (what, id_num))
        rv = self.app.post(
            modify_url,
            data=form_data,
        )
        # TODO success

        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"xxxx": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)

       # csv
        fh = open("config.json", "r")
        config_data = json.load(fh)
        fh.close()

        self.assertDictEqual(expected_json, rv)


    def test_modify_config_build_other(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success
    
        # json_string, what, id_num
        what = "build"
        id_num = 1
        form_data = {"build_id": 1, "target_name": "endor16", "other_target_name": "test"} # submit <form>
        modify_url = ("/edit/config/modify/%s/%s" % (what, id_num))
        rv = self.app.post(
            modify_url,
            data=form_data,
        )
        # TODO success

        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"endor16": {"OTHER": "test", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)

       # csv
        fh = open("config.json", "r")
        config_data = json.load(fh)
        fh.close()

        self.assertDictEqual(expected_json, rv)


    def test_modify_config_target(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success
    
        # json_string, what, id_num
        what = "target"
        id_num = 1
        # there is no configs stuff, so get crud.py to leave it be [keys]
        form_data = {"target_id": 1, "target_mode": "aaaa", "base_image": "bbbb", "repo": "cccc", "parent_target_name": "endor16"} # submit <form>
        modify_url = ("/edit/config/modify/%s/%s" % (what, id_num))
        rv = self.app.post(
            modify_url,
            data=form_data,
        )
        # TODO success

        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"endor16": {"OTHER": "sg16", "aaaa": {"base": "bbbb", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "cccc"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)

       # csv
        fh = open("config.json", "r")
        config_data = json.load(fh)
        fh.close()

        self.assertDictEqual(expected_json, rv)


    def test_modify_config_target_configs(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success
    
        # json_string, what, id_num
        what = "target"
        id_num = 1
        # there is no configs stuff, so get crud.py to leave it be [keys]
        form_data = {"target_id": 1, "target_mode": "aaaa", "base_image": "bbbb", "repo": "cccc", "parent_target_name": "endor16", "config_keys": '["other_stuff", "template"]', "other_stuff": "ssss", "template": "ffff"} # submit <form>
        modify_url = ("/edit/config/modify/%s/%s" % (what, id_num))
        rv = self.app.post(
            modify_url,
            data=form_data,
        )
        # TODO success

        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"endor16": {"OTHER": "sg16", "aaaa": {"base": "bbbb", "configs": {"other_stuff": "ssss", "template": "ffff"}, "repo": "cccc"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)

       # csv
        fh = open("config.json", "r")
        config_data = json.load(fh)
        fh.close()

        self.assertDictEqual(expected_json, rv)


    def test_modify_config_target_relationship(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success
    
        # json_string, what, id_num
        what = "target"
        id_num = 1
        # there is no configs stuff, so get crud.py to leave it be [keys]
        form_data = {"target_id": 1, "target_mode": "aaaa", "base_image": "bbbb", "repo": "cccc", "parent_target_name": "test"} # submit <form>
        modify_url = ("/edit/config/modify/%s/%s" % (what, id_num))
        rv = self.app.post(
            modify_url,
            data=form_data,
        )
        # TODO success

        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"endor16": {"OTHER": "sg16", "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "aaaa": {"base": "bbbb", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "cccc"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)

       # csv
        fh = open("config.json", "r")
        config_data = json.load(fh)
        fh.close()

        self.assertDictEqual(expected_json, rv)


    def test_remove_host(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success
    
        # json_string, what, id_num
        what = "host"
        id_num = 1
        # there is no configs stuff, so get crud.py to leave it be [keys]
        remove_url = ("/remove/%s/%s" % (what, id_num))
        rv = self.app.get(
            remove_url,
        )
        # TODO success

        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host":  {"server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)

       # csv
        fh = open("config.json", "r")
        config_data = json.load(fh)
        fh.close()

        self.assertDictEqual(expected_json, rv)


    def test_remove_build(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success
    
        # json_string, what, id_num
        what = "build"
        id_num = 1
        # there is no configs stuff, so get crud.py to leave it be [keys]
        remove_url = ("/remove/%s/%s" % (what, id_num))
        rv = self.app.get(
            remove_url,
        )
        # TODO success

        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)

       # csv
        fh = open("config.json", "r")
        config_data = json.load(fh)
        fh.close()

        self.assertDictEqual(expected_json, rv)


    def test_remove_target(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success
    
        # json_string, what, id_num
        what = "target"
        id_num = 1
        # there is no configs stuff, so get crud.py to leave it be [keys]
        remove_url = ("/remove/%s/%s" % (what, id_num))
        rv = self.app.get(
            remove_url,
        )
        # TODO success

        expected_config = fetch_config(self.app)

        expected_json = {"builds": {"endor16": {"OTHER": "sg16", "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}

        self.assertEqual(200, expected_config.status_code)
        # print("****rv %s is type %s" % (expected_config.data,type(expected_config.data)))
        rstring = expected_config.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_json, rv)

       # csv
        fh = open("config.json", "r")
        config_data = json.load(fh)
        fh.close()

        self.assertDictEqual(expected_json, rv)


    def tearDown(self):
        # clear db between tests
        db.session.remove()
        db.drop_all()


# request  - decode - fetch
class TestRDF(unittest.TestCase):
    def setUp(self):
        # Assign the flask app object
        app = config_app("testing", flask_app)
        with app.app_context():
           db.create_all()

        self.app = app.test_client()
        self.db = db

        self.maxDiff = None

        os.system("cp build_wrapper/test/config.json config.json")


    def test_request(self):
        given = {"builds": {"endor16": {"OTHER": "sg16", "trusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<16>"}, "untrusted": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah0", "template": "untrusted.json"}, "repo": "/storage/repo/<16>"}}, "endor18": {"OTHER": "sg18", "trusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah", "template": "trusted.json"}, "repo": "/storage/repo/<18>"}, "untrusted": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah2", "template": "untrusted.json"}, "repo": "/storage/repo/<18>"}}, "endor20": {"OTHER": "sg20", "trusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah3", "template": "trusted.json"}, "repo": "/storage/repo/<20>"}, "untrusted": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah4", "template": "untrusted.json"}, "repo": "/storage/repo/<20>"}}, "sg16": {"client": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah5", "template": "sg16_client.json"}, "repo": "/storage/repo/<16>"}, "recovery": {"base": "images/lubuntu16.img", "configs": {"other_stuff": "blah6", "template": "sg16_recovery.json"}, "repo": "/storage/repo/<16>"}}, "sg18": {"client": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah7", "template": "sg18_client.json"}, "repo": "/storage/repo/<18>"}, "recovery": {"base": "images/lubuntu18.img", "configs": {"other_stuff": "blah8", "template": "sg18_recovery.json"}, "repo": "/storage/repo/<18>"}}, "sg20": {"client": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah9", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}, "recovery": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah10", "template": "sg20_recovery.json"}, "repo": "/storage/repo/<20>"}}, "test": {"one_thing": {"base": "images/lubuntu20.img", "configs": {"other_stuff": "blah20", "template": "sg120_client.json"}, "repo": "/storage/repo/<20>"}}}, "global": {"build_host": {"laptop001": {"known_hubs": ["foo1: foo2", "bar1: bar2"]}, "server002": {"known_hubs": ["Bus 002 Device 002: ID 8087:8002 Intel Corp. ", "Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 001 Device 002: ID 8087:800a Intel Corp. ", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 008: ID 1532:0064 Razer USA, Ltd ", "Bus 003 Device 005: ID 05e3:0608 Genesys Logic, Inc. Hub", "Bus 003 Device 007: ID 0c45:7605 Microdia Das Keyboard", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]}, "home": {"known_hubs": ["Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 003 Device 004: ID 0408:3046 Quanta Computer, Inc. ", "Bus 003 Device 003: ID 13d3:3496 IMC Networks ", "Bus 003 Device 002: ID 04f3:0903 Elan Microelectronics Corp. ", "Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub", "Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub", "Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver", "Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub"]} }, "use_case": "reuse", "use_mode": "stand-alone"}}
        rv = load_config(self.app, given) # json in to sql
        # TODO success

        request_data = {"mode": "stand-alone", "where": "home", "username": "user", "password": "password",  "todo_name": "sg16", "use_case": "reuse", "submit_test": "For Test"} # submit <form>
        request_url = ("/edit/request/make")
        rv = self.app.post(
            request_url,
            data=request_data,
        )
        # For Test
        # "For Production" -> "NEXT"  (hidden data)

        expected_request = "curl --request POST -H 'Content-Type: application/json' -d '{\"mode\": \"stand-alone\", \"where\": \"home\", \"username\": \"user\", \"password\": \"password\", \"todo\": [\"sg16\"], \"use_case\": \"reuse\", \"test_flag\": \"True\"}' http://localhost/decode"

        self.assertEqual(200, rv.status_code)
        # message <html>
        html_string = rv.data
        soup = BeautifulSoup(html_string.decode('utf-8'), 'html.parser')
        message = soup.find('div', attrs={'class': 'message'})
        ####DEBUG message <div class="message">curl --request POST -H 'Content-Type: application/json' -d '{"mode": "stand-alone", "where": "server002", "username": "user", "password": "password", "todo": ["sg16"], "use_case": "reuse", "test_flag": "True"}' http://localhost/decode</div> is type <class 'bs4.element.Tag'>
        self.assertEqual(str(expected_request), str(message.text.strip()))


    def test_decode_fetch(self):
        # testing - no usb, and hostname='foo'
        decode_data = {"mode": "stand-alone", "where": "home", "username": "user", "password": "password", "todo": ["sg16"], "use_case": "reuse", "unittest_flag": "True"}
        decode_url = ("/decode")
        rv = self.app.post(
            decode_url,
            data=json.dumps(decode_data),
            content_type='application/json',
        )
        # no usb drives -- date, session name, and target_id
        expected_decode = {"common": {"foo": [{"build_disk_img": "run/sg16_client.img", "configs": "{'other_stuff': 'blah5', 'template': 'sg16_client.json'}", "ip": "192.168.1.130", "password": "password", "repo": "/storage/repo/<16>", "request_time": "None", "target": "sg16_client", "target_hostname": "foo", "target_id": "1", "updatable_build_output": "None", "updatable_datetime_end": "None", "updatable_datetime_start": "None", "updatable_exit_code": "None", "updatable_reason": "None", "updatable_recover_key": "None", "updatable_usb_hw_info": "None", "usb_address": "***.***", "username": "user", "where": "home"}, {"build_disk_img": "run/sg16_recovery.img", "configs": "{'other_stuff': 'blah6', 'template': 'sg16_recovery.json'}", "ip": "192.168.1.131", "password": "password", "repo": "/storage/repo/<16>", "request_time": "None", "target": "sg16_recovery", "target_hostname": "foo", "target_id": "2", "updatable_build_output": "None", "updatable_datetime_end": "None", "updatable_datetime_start": "None", "updatable_exit_code": "None", "updatable_reason": "None", "updatable_recover_key": "None", "updatable_usb_hw_info": "None", "usb_address": "***.***", "username": "user", "where": "home"}]}, "messages": {"done": ["lsusb"], "info": ["****WARN: too few USB drives -- expected: 2 plugged in: 0", "NOTE: common object for us to share in run/common.json, commands in [\"messages\"][\"todo\"]"], "todo": ['cp images/lubuntu16.img run/sg16_client.img','cp images/lubuntu16.img run/sg16_recovery.img'], "todo_create_kvm": ["virt-install --virt-type kvm --name home_foo_sg16_client  --ram 8192 --disk run/sg16_client.img --graphics vnc,listen=0.0.0.0 --noautoconsole --check path_in_use=off --boot hd --hostdev ***.*** --network bridge=virbr0,mac='52:54:00:01:82:00' --video vga", "virt-install --virt-type kvm --name home_foo_sg16_recovery  --ram 8192 --disk run/sg16_recovery.img --graphics vnc,listen=0.0.0.0 --noautoconsole --check path_in_use=off --boot hd --hostdev ***.*** --network bridge=virbr0,mac='52:54:00:01:82:01' --video vga"], "todo_inside_kvm": ["udevadm info /dev/sda | grep 'ID_SERIAL='"]}}

        rstring = rv.data
        rv = json.loads(rstring.decode('utf-8'))
        self.assertDictEqual(expected_decode, rv)
        
        # ???? TODO fetch


    def tearDown(self):
        # clear db between tests
        db.session.remove()
        db.drop_all()


if __name__ == "__main__":
        # config.json - only want todo this once
        os.system("mv config.json config.json.pre_test")

        unittest.main()

        os.system("cp config.json config.json.pre_test")
