#!/usr/bin/python3
import json, sys


# print(sys.argv[1]+ "\n\n")
fh = open(sys.argv[1], "r")
safe_string = json.load(fh)

# json_string = json.dumps(safe_string, indent=2, sort_keys=True)
print(json.dumps(safe_string, sort_keys=True))
