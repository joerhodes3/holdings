# -*- coding: utf-8 -*-
from sqlalchemy import (
    Column,
    Date,
    Text,
    String,
    Integer,
    Float,
    Boolean,
    ForeignKey,
    create_engine,
)
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from datetime import date
import os


db_engine = "sqlite:///holding/assets_testing.sql"
if os.environ.get("HOLDINGS_MODE") == "production":
    db_engine = "sqlite:///holding/assets_production.sql"
    print(".......in prodution")
elif os.environ.get("HOLDINGS_MODE") == "development":
    db_engine = "sqlite:///holding/assets_development.sql"
    print(".......in development")
print(f"\t{db_engine}")

engine = create_engine(db_engine, convert_unicode=True)
db_session = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine)
)

Base = declarative_base()
# We will need this for querying
Base.query = db_session.query_property()


class ledger(Base):
    # SQLAlchemy Model for everything (have, taxable[transactions are seperated into have and taxable], todo

    __tablename__ = "ledger"
    id = Column(Integer, primary_key=True)

    Operation = Column(String(16))
    Asset = Column(String(8))

    # cost basis
    Date_bought = Column(Date, default=date.today())
    Price_bought = Column(Float, default=0.0)

    Amount = Column(Float, default=0.0)
    Date_sold = Column(Date, default=date.today())
    Price_sold = Column(Float, default=0.0)

    term = Column(String(8), default=None)
    proceeds = Column(Float, default=0.0)

    is_todo_sell = Column(Boolean, default=False)
    is_todo_buy = Column(Boolean, default=False)
    is_have = Column(Boolean, default=False)
    is_taxable = Column(Boolean, default=False)
    was_processed = Column(Boolean, default=False)

    transaction_fkey = Column(Integer, ForeignKey("ledger.id"), default=None)
    request_fkey = Column(Integer, ForeignKey("ledger.id"), default=None)

    # notes to help me
    purchased_on = Column(String(64))
    held_on = Column(String(64))
    sold_on = Column(String(64))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class comment(Base):
    # SQLAlchemy Model for extra text relating to stuff on the ledger

    __tablename__ = "comment"
    id = Column(Integer, primary_key=True)

    # Optional
    Asset = Column(String(8))
    text = Column(Text)
    is_visable = Column(Boolean, default=False)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


# function to make a dict out of row of SQL data
def serialize(model):
    columns = {}
    for key, value in model.items():
        columns[key] = SerializeDate(value)
    return dict(columns)


# function to make a str out of datatime() by eval()
def SerializeDate(value):
    # if the type is not a str, then Serialize
    if isinstance(value, date):
        item = value.strftime("%m/%d/%Y")
    else:
        item = str(value)

    return item
