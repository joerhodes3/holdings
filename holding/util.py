# -*- coding: utf-8 -*-
import csv, json
from datetime import date
from dateutil.relativedelta import relativedelta

from io import StringIO
from copy import deepcopy

from holding.models import (
    db_session,
    ledger,
    comment,
)


def date_view(Date):
    # in:
    # - in a Date from csv
    # - yyyy-mm-dd or mm/dd/yyyy
    #
    # out:
    # - a datetime object
    if Date == "???":
        return None
    elif "-" in Date:
        YYYY, MM, DD = Date.split("-")
        return date(int(YYYY), int(MM), int(DD))
    elif "/" in Date:
        MM, DD, YYYY = Date.split("/")
        return date(int(YYYY), int(MM), int(DD))

    return None


class asset_items:
    def __init__(self):
        # dict of list of dict
        # {asset:[{buy}, {buy}, {buy}], ... }

        # sort SQL into Asset by Date for FIFO
        self.FIFO = {}
        self.have2stuff()  # populate self.FIFO

        self.index = 1
        print("FIFO:")
        print(str(self.FIFO))
        print()

    def have2stuff(self):
        all_haves = ledger.query.filter_by(is_have=True).all()

        index = {}
        # SQL to things.stuff[] -- to build a FIFO per asset
        for item in all_haves:
            if item.Asset not in self.FIFO:
                self.FIFO[item.Asset] = []
                index[item.Asset] = 0

            self.FIFO[item.Asset].append({})
            self.FIFO[item.Asset][index[item.Asset]].update(
                {
                    "have_pid": item.id,
                    "Date_bought": item.Date_bought,
                    "Price_bought": item.Price_bought,
                    "Amount": item.Amount,
                }
            )

            index[item.Asset] += 1

        db_session.commit()

    def buy(self, date_object, Asset, Amount, Price, Exchange):
        # add to have on ledger and build FIFO (stuff)
        # print(f"##DEBUG buy Asset {Asset} on Date {str(date_object)}")
        if date_object:
            transaction = {
                "Date_bought": date_object,
                "Amount": Amount,
                "Price_bought": Price,
            }
        else:
            transaction = {
                "Date_bought": "???",
                "Amount": Amount,
                "Price_bought": Price,
            }
        # NOT a taxable event, is a transaction that is already logged.......
        # new item in SQL
        transaction.update({"Operation": "BUY"})
        transaction.update({"purchased_on": Exchange})
        transaction.update({"held_on": Exchange})
        transaction.update({"Asset": Asset})
        transaction.update({"is_have": True})

        ledger_row = ledger(**transaction)
        db_session.add(ledger_row)
        db_session.commit()
        db_session.refresh(ledger_row)
        have_pid = ledger_row.id

        # Also, put in stuff[]
        if Asset in self.FIFO:
            # asset exists
            buy_list = deepcopy(self.FIFO[Asset])
            index = 0
            for item in buy_list:
                # sort -- currently newest, mean for oldest fist -- FIFO!!!!
                # find index
                # print(str(date_object)+" -- "+str(item["Date_bought"]))
                if date_object:
                    Date = str(date_object)
                    # sort Date in csv
                    if Date < str(item["Date_bought"]):
                        # incoming earlier, insert before here
                        break
                    elif Date > str(item["Date_bought"]):
                        # incoming date later -- keep walking
                        index += 1
                else:
                    # put unknown date (ADD) at end
                    index = len(buy_list)
                    break
            # save updated list with new transaction
            item["have_pid"] = have_pid
            item["Date_bought"] = date_object
            item["Amount"] = Amount
            item["Price_bought"] = Price
            self.FIFO[Asset].insert(index, deepcopy(item))
        else:
            # first asset of that type
            self.FIFO[Asset] = []

            item = transaction
            item["have_pid"] = have_pid
            item["Date_bought"] = date_object
            item["Amount"] = Amount
            item["Price_bought"] = Price
            self.FIFO[Asset].append(deepcopy(item))

    def sell(self, date_object, Asset, Amount, Price, Exchange, request_pid):
        # print(f"##DEBUG sell Asset {Asset} on Date {str(date_object)}")
        if date_object:
            Date = date_object
        else:
            Date = "???"

        if Asset in self.FIFO:
            # found asset
            total = Amount
            price_per = Price / Amount
            while total != float(0):
                try:
                    have_pid = self.FIFO[Asset][0]["have_pid"]
                    del self.FIFO[Asset][0]["have_pid"]
                except:
                    print(f"  Not Found Asset {Asset} on Date {str(date_object)}")

                if self.FIFO.get(Asset) is None:
                    print(f"WARN: {Asset} not in fifo -- look at prev year HAVE")
                    exit(255)
                else:
                    print(f"About to process {Asset} for sale")
                if total < float(self.FIFO[Asset][0]["Amount"]):
                    # subtract as much as specified in total, amount to be sold smaller than FIFO
                    old_have = ledger.query.filter_by(id=have_pid).one()

                    old_total = total  # save value
                    total = 0.0  # completely processed

                    bought_price = (old_have.Price_bought / old_have.Amount) * old_total
                    sell_price = price_per * old_total

                    total_left = float(self.FIFO[Asset][0]["Amount"]) - old_total
                    total_price = (old_have.Price_bought / old_have.Amount) * total_left

                    # <new_have> amount we still hold
                    have_dict = {
                        "Operation": "BUY",
                        "is_have": True,
                        "Date_bought": old_have.Date_bought,
                        "Asset": old_have.Asset,
                        "Amount": total_left,
                        "Price_bought": total_price,
                        "purchased_on": old_have.purchased_on,
                        "held_on": old_have.held_on,
                    }
                    new_have = ledger(**have_dict)
                    db_session.add(new_have)
                    db_session.commit()
                    db_session.refresh(new_have)
                    new_have_pid = new_have.id

                    # update self.FIFO[Asset][index] in original [asset] at HEAD of list
                    self.FIFO[Asset][0]["Price_bought"] = total_price
                    self.FIFO[Asset][0]["Amount"] = total_left  # should never be 0

                    event_row = ledger(**self.FIFO[Asset][0])
                    event_row.Date_bought = old_have.Date_bought  # why not in FIFO?
                    event_row.is_taxable = True
                    event_row.transaction_fkey = have_pid
                    event_row.request_fkey = request_pid
                    event_row.Date_sold = Date
                    event_row.sold_on = Exchange  # notes
                    event_row.Asset = Asset
                    event_row.Operation = "SELL"
                    event_row.Amount = old_total
                    event_row.Price_bought = bought_price
                    event_row.Price_sold = sell_price
                    db_session.add(event_row)
                    db_session.commit()
                    db_session.refresh(event_row)
                    event_pid = event_row.id

                    self.FIFO[Asset][0]["have_pid"] = new_have_pid

                    # update
                    old_have.is_have = False
                    old_have.transaction_fkey = event_pid
                    db_session.add(old_have)
                    db_session.commit()

                    if date_object == None:
                        event_row.term = "unknown"
                    elif (
                        relativedelta(
                            date_object, self.FIFO[Asset][0]["Date_bought"]
                        ).years
                        > 0
                    ):
                        event_row.term = "long"
                    else:
                        event_row.term = "short"

                    event_row.proceeds = event_row.Price_sold - event_row.Price_bought

                    db_session.add(event_row)
                    db_session.commit()
                else:
                    # the SELL demand is bigger or equal to the self.FIFO[Asset][index] -- rm this self.FIFO[Asset][index] (add SELL transaction / rm BUY and adjust total)
                    old_have = ledger.query.filter_by(id=have_pid).one()

                    sell_price = price_per * self.FIFO[Asset][0]["Amount"]
                    if total == self.FIFO[Asset][0]["Amount"]:
                        total = float(0)
                    else:
                        total -= self.FIFO[Asset][0]["Amount"]

                    event_row = ledger(**self.FIFO[Asset][0])
                    event_row.Date_bought = old_have.Date_bought  # why not in FIFO?
                    event_row.Price_bought = old_have.Price_bought  # why not in FIFO?
                    event_row.Amount = self.FIFO[Asset][0][
                        "Amount"
                    ]  # redo amount in terms of the old total -- amount we remove from FIFO
                    event_row.is_taxable = True
                    event_row.transaction_fkey = have_pid
                    event_row.request_fkey = request_pid
                    event_row.is_have = False
                    event_row.Date_sold = Date
                    event_row.sold_on = Exchange  # notes
                    event_row.Asset = Asset
                    event_row.Operation = "SELL"
                    event_row.Price_sold = sell_price
                    db_session.add(event_row)
                    db_session.commit()
                    db_session.refresh(event_row)
                    event_pid = event_row.id

                    self.FIFO[Asset].pop(0)

                    # <old have> amount we sold
                    old_have.is_have = False
                    old_have.transaction_fkey = event_pid
                    db_session.add(old_have)

                    if date_object == None:
                        event_row.term = "unknown"
                    elif relativedelta(date_object, event_row.Date_bought).years > 0:
                        event_row.term = "long"
                    else:
                        event_row.term = "short"

                    if Price != 0.0:
                        event_row.proceeds = (
                            event_row.Price_sold - event_row.Price_bought
                        )

                        # only figure tax / event if there is a cost basis (Price of 0.0 is "LOSE")
                        db_session.add(event_row)
                        db_session.commit()

                if total == float(0):
                    print("zero")

            if total < 0:
                print("Error -- more to be sold than have")
                exit(0)
        else:
            print(f"Error trying to sell non-existant asset {Asset} on {Date}")

    def interest(self, date_object, Asset, Amount, Price, Exchange):
        t2 = {}
        t2.update({"is_taxable": True})
        t2.update({"Date_sold": date_object, "Date_bought": date_object})
        t2.update({"Asset": Asset, "Operation": "INTEREST"})
        t2.update({"Amount": Amount, "Price_sold": Price, "Price_bought": 0})
        t2.update({"term": "short"})  # all interest is short term
        ledger_row = ledger(**t2)
        db_session.add(ledger_row)
        db_session.commit()

    def airdrop(self, date_object, Asset, Amount, Price, Exchange):
        t2 = {}
        t2.update({"is_taxable": True})
        t2.update({"Date_sold": date_object, "Date_bought": date_object})
        t2.update({"Asset": Asset, "Operation": "AIRDROP"})
        t2.update({"Amount": Amount, "Price_sold": Price, "Price_bought": 0})
        t2.update({"term": "short"})  # all airdrop is short term
        ledger_row = ledger(**t2)
        db_session.add(ledger_row)
        db_session.commit()

    # ops -- excute from HAVE/BUY/SELL/INTEREST/LOSE & AIRDROP (like INTEREST but different [asset] to BUY)
    def process_todo(self):
        all_todos = []
        all_todos.append(ledger.query.filter_by(is_todo_buy=True).all())
        all_todos.append(ledger.query.filter_by(is_todo_sell=True).all())

        for todo_obj in all_todos:
            for todo in todo_obj:
                if todo.Operation == "HAVE" or todo.Operation == "BUY":
                    self.buy(
                        todo.Date_bought,
                        todo.Asset,
                        todo.Amount,
                        todo.Price_bought,
                        todo.purchased_on,
                    )
                    todo.is_todo_buy = False
                    todo.was_processed = True
                elif todo.Operation == "INTEREST":
                    self.interest(
                        todo.Date_bought,
                        todo.Asset,
                        todo.Amount,
                        todo.Price_bought,
                        todo.held_on,
                    )
                    self.buy(
                        todo.Date_bought,
                        todo.Asset,
                        todo.Amount,
                        todo.Price_bought,
                        todo.held_on,
                    )
                    todo.is_todo_buy = False
                    todo.was_processed = True
                elif todo.Operation == "AIRDROP":
                    self.airdrop(
                        todo.Date_bought,
                        todo.Asset,
                        todo.Amount,
                        todo.Price_bought,
                        todo.held_on,
                    )
                    self.buy(
                        todo.Date_bought,
                        todo.Asset,
                        todo.Amount,
                        todo.Price_bought,
                        todo.held_on,
                    )
                    todo.is_todo_buy = False
                    todo.was_processed = True
                elif todo.Operation == "SELL":
                    self.sell(
                        todo.Date_sold,
                        todo.Asset,
                        todo.Amount,
                        todo.Price_sold,
                        todo.sold_on,
                        todo.id,
                    )
                    todo.is_todo_sell = False
                    todo.was_processed = True
                elif todo.Operation == "ADD" or Operation == "BUY":
                    self.buy("???", todo.Asset, todo.Amount, "???", "???")
                    todo.is_todo_buy = False
                    todo.was_processed = True
                elif todo.Operation == "LOSE":
                    self.sell(todo.Date_sold, todo.Asset, todo.Amount, 0.0, "???")
                    todo.is_todo_sell = False
                    todo.was_processed = True
                else:
                    print(f"Unknown Operation: {todo.Operation} id: {todo.id}")
                db_session.add(todo)

        db_session.commit()


def parse_csv(csv_stream, header_style, header):
    # in:
    # - StringIO -- a CSV with a header -- file in memory
    # - howto parse file into ops (header_style)
    #
    # out:
    # - nothing is stored in SQL
    if header_style == "have":
        # with header -- Date_bought,Operation,Asset,Amount,Price_bought,purchased_on,held_on
        csv_reader = list(
            csv.DictReader(StringIO(csv_stream.getvalue()), lineterminator="\n")
        )
        index = 1
        for row in csv_reader:
            # skip comments????
            # prescreen incoming csv to make sure only legal Operations
            if not (row["Operation"] == "HAVE"):
                print(
                    f"Bad opperand {row['Operation']} is not supported in mode {header_style}"
                )
                exit(0)
            else:
                date_object = date_view(row["Date_bought"])

                have_dict = {
                    "Operation": "BUY",
                    "is_have": True,
                    "id": index,
                    "Date_bought": date_object,
                    "Asset": row["Asset"],
                    "Amount": row["Amount"],
                    "Price_bought": row["Price_bought"],
                    "purchased_on": row["purchased_on"],
                    "held_on": row["held_on"],
                }
                have_row = ledger(**have_dict)
                print(f"DEBUG: {have_row.as_dict()}")
                db_session.add(have_row)

                index += 1

            db_session.commit()

    elif header_style == "year" or header_style == "audit":
        # with header -- Date,Operation,Asset,Amount,Price,Exchange
        csv_reader = list(
            csv.DictReader(StringIO(csv_stream.getvalue()), lineterminator="\n")
        )
        index = 1
        for row in csv_reader:
            # prescreen incoming csv to make sure only legal Operations
            if not (
                row["Operation"] == "BUY"
                or row["Operation"] == "SELL"
                or row["Operation"] == "INTEREST"
                or row["Operation"] == "AIRDROP"
                or row["Operation"] == "LOSE"
                or row["Operation"] == "ADD"
            ):
                # ?? year and audit have different allowed operations ??
                print(
                    f"Bad opperand {row['Operation']} is not supported in mode {header_style}"
                )
                exit(0)
            else:
                # do not use index here as <transactions> are previosly there
                todo_dict = {
                    "Operation": row["Operation"],
                    "Asset": row["Asset"],
                    "Amount": row["Amount"],
                }
                if row["Operation"] == "SELL" or row["Operation"] == "LOSE":
                    todo_dict.update(
                        {
                            "Date_sold": date_view(row["Date"]),
                            "Price_sold": row["Price"],
                            "sold_on": row["Exchange"],
                            "is_todo_sell": True,
                        }
                    )
                else:
                    todo_dict.update(
                        {
                            "Date_bought": date_view(row["Date"]),
                            "Price_bought": row["Price"],
                            "purchased_on": row["Exchange"],
                            "is_todo_buy": True,
                        }
                    )
                todo_row = ledger(**todo_dict)
                db_session.add(todo_row)
            db_session.commit()

    elif header_type == "long":
        # expect odd format and unwind into buy/sell events
        pass
    elif header_type == "short":
        # expect odd format and unwind into buy/sell events
        pass
    else:
        print("Error unknown input format")
        exit(0)


def process_todo():
    things = (
        asset_items()
    )  # init an asset object to process -- clear <events> and <have> after loading them
    things.process_todo()


class report:
    def __init__(self, assets_json):
        self.assets_json = assets_json

    def process_assets(self):
        # modify self.assets_json in place to add SUBTOTALS to each asset type
        #
        # only makes sense for assets you still HAVE
        lazy_asset = None
        lazy_subtotal = 0.0

        lines = []
        for asset_line in json.loads(self.assets_json):
            if lazy_asset == None:
                lazy_asset = asset_line["Asset"]
                lazy_paid = 0

            if lazy_asset != asset_line["Asset"]:
                lines.append(
                    {
                        "Operation": "SUBTOTAL",
                        "Asset": lazy_asset,
                        "Amount": lazy_subtotal,
                        "Total": lazy_paid,
                        "Average": lazy_paid / lazy_subtotal,
                    }
                )
                lines.append(asset_line)

                lazy_asset = asset_line["Asset"]
                lazy_subtotal = float(asset_line["Amount"])
                lazy_paid = float(asset_line["Price_bought"])
            else:
                lines.append(asset_line)

                lazy_subtotal += float(asset_line["Amount"])
                lazy_paid += float(asset_line["Price_bought"])
            print(lazy_paid)

        lines.append(
            {
                "Operation": "SUBTOTAL",
                "Asset": lazy_asset,
                "Amount": lazy_subtotal,
                "Total": lazy_paid,
                "Average": lazy_paid / lazy_subtotal,
            }
        )
        self.assets_json = json.dumps(lines)

    def calc_tax_total_gain(self):
        # modify self.assets_json in place with TOTAL of proceeds for short/long at end
        #
        # only makes sense for tax
        totals = {}
        totals["long"] = 0
        totals["short"] = 0
        totals["unknown"] = 0
        tax_obj = json.loads(self.assets_json)
        lines = []
        for item in tax_obj:
            lines.append(item)
            totals[item["term"]] += float(item["proceeds"])
        lines.append(
            {
                "Operation": "TOTAL",
                "amount_short": totals["short"],
                "amount_long": totals["long"],
                "amount_unknown": totals["unknown"],
            }
        )
        self.assets_json = json.dumps(lines)

    def json_to_doc_asset(self, keys):
        # in:
        # - JSON string -- in self.assets_json
        # - keys
        #
        # out:
        # - return "[{header: "", table: [<rows [<cells>]>], footer: ""}, ...]"
        items = json.loads(self.assets_json)
        self.assets_json = None  # reclaim memory
        table = []
        header = []
        header.append(keys)
        document = [{"header": "HAVE", "table": header, "footer": ""}]
        for item in items:
            if item["Operation"] == "BUY":
                item["Operation"] = "HAVE"
                rows = []
                for key in keys:
                    rows.append(item[key])
                table.append(rows)

            elif item["Operation"] == "SUBTOTAL":
                if item["Amount"] > 0.00001:
                    subtotal = f'SUBTOTAL OF {item["Asset"]} AMOUNT: {item["Amount"]} TOTAL PAID: {item["Total"]} PRICE PER COIN: {item["Average"]}'

                    document.append(
                        {
                            "header": item["Asset"],
                            "table": deepcopy(table),
                            "footer": subtotal,
                        }
                    )

                table = []  # reset

        return document

    def json_to_doc_tax(self, keys):
        # in:
        # - JSON string -- in self.assets_json
        # - keys
        #
        # out:
        # - return "[{header: "", table: [<rows [<cells>]>], footer: ""}, ...]"
        items = json.loads(self.assets_json)
        self.assets_json = None  # reclaim memory
        tables = {}
        tables["SELL"] = []
        tables["SELL"].append(keys)
        tables["INTEREST"] = []
        tables["INTEREST"].append(keys)
        tables["AIRDROP"] = []
        tables["AIRDROP"].append(keys)
        for item in items:
            if item["Operation"] != "TOTAL":
                rows = []
                for key in keys:
                    rows.append(item[key])
                tables[item["Operation"]].append(deepcopy(rows))
            else:
                total = f'TOTAL GAINS    short: {item["amount_short"]} long: {item["amount_long"]} unknown: {item["amount_unknown"]}'

        document = []
        document.append({"header": "SELL", "table": tables["SELL"], "footer": ""})
        document.append(
            {"header": "INTEREST", "table": tables["INTEREST"], "footer": ""}
        )
        document.append({"header": "AIRDROP", "table": tables["AIRDROP"], "footer": ""})
        document.append({"header": "", "table": [], "footer": total})

        return document

    def json_to_txf_sell(self, fname):
        # in:
        # - JSON string -- in self.assets_json
        # - fname to output to
        #
        # out:
        # - a txf of SELL for the Form 8849
        # TODO AIRDROP and ?INTEREST?
        FileOut = open(fname, "w")

        items = json.loads(self.assets_json)
        FileOut.write("V042\n")
        FileOut.write("holdings by Joe Rhodes\nD")
        FileOut.write(date.today().strftime("%m/%d/%Y"))
        FileOut.write("\n")
        line = "^\n"
        for item in items:
            if item["Operation"] == "SELL":
                if item["term"] == "long":
                    taxRef = 323
                else:
                    taxRef = 321

                line += f"TD\nN{taxRef}\nC1\nL1\n"
                line += f'P{item["Amount"]} {item["Asset"]}\n'
                line += f'D{item["Date_bought"]}\n'
                line += f'D{item["Date_sold"]}\n'
                line += f'${item["Price_bought"]}\n'
                line += f'${item["Price_sold"]}\n'

                FileOut.write(line)
                line = "^\n"
            else:
                continue

        FileOut.write("^\n")
        FileOut.close()

    def output_json(self, fname):
        # in:
        # - JSON string -- in self.assets_json
        # - fname to output to
        #
        # out:
        # - a JSON file
        FileOut = open(fname, "w")
        FileOut.write(json.dumps(self.assets_json))
        FileOut.close()
