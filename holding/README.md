# holdings

# about/ -- is for tracking assets held (crypto works just like security -- intertest/staking proceed taxed as they accrue, and then are cost basis)

# TODO:
- everything goes to db -- Flask API (have and year)
- process (year -- 2021)
- report

# other posible stuff post Flask
- GraphQL
- security (keycloak)
- CI/CD
- scan (Sonartype linter)
 
# prereq
 the code is all python 3.8
 
- for the modles.py to access the local sqlite3 db use sqlalchamy as an ORM
    + sudo apt install sqlite3 python3-flask_sqlalchemy
- for web.py use flask and swagger
    + sudo apt install python3-flask
    + sudo apt install python3-flasgger
- forms for flask templates
    + sudo apt install python3-wtforms
    + sudo apt install python3-flaskext.wtf

- Rocky Linux:
```
sudo pip3 install --upgrade pip

pip install flask_sqlalchemy
pip install flask
pip install flasgger
pip install wtforms
pip install flask_Wtf
```

# python
===================== web.py ===================
- the highest level (api -- source and dest)
    + is a Flask app with swagger doc for all the API
    + uses templates / forms to collect data

# +++++++++++++++++++++ explain files +++++++++++++++++++++
- documentation
    + README.md -- description of all the things
- unit test, not runtime
    + test/ -- dir to hold all unit test for code that build the builer VM
- initial csv
    + 2020_assets_after.csv - is all 2020 transactions stated in terms of HAVE
- transactions
    + either a year like 2021.csv or an 2021_audit.csv to specify corrections/loses
- lowest level
    + util.py -- common py routines
    + forms.py -- common form elements for web.py to use through the Flask templates
    + models.py -- descibe common object that is shared
- highest level this is the friendly face to interact with
    + web.py -- flask and swagger -- api entrypoints to stuff
    + templates/edit_index.html -- this is what allows us to chose what to edit
    + templates/edit_config.html -- this is what allows 'global' and 'build' in the config.json
    + templates/edit_request.html -- this is what allows us to build the request that is used in the build -- we make this to be nice

- local db (sqlite3)
    + assets.sql -- db that has all the effectss of the transactions
