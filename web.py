# -*- coding: utf-8 -*-
# use vitualenv and install from requirements: pip -r requirements.txt
import os
os.environ['HOLDINGS_MODE'] = "development"

from holding import flask_app, config_app, shutdown_session


# TODO alternative runner for prod -- gnunicorn?
if __name__ == "__main__":
    main_app = config_app(os.environ.get('HOLDINGS_MODE'), flask_app)
    main_app.run(host="127.0.0.1", port=5002, debug=True)
